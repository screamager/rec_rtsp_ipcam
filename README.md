# rec_rtsp_ipcam

Recording RTSP streams from IP cameras as m3u8 with short switches of files and concatenation them to mp4 files in dirs by dates.

PLUSES: almost seamless recording and at break connect minimal lose data.

MINUSES: additional load on disk due to copy-paste at concatenation and high count short files.
It's problems may be resolved by RAM and mounting dir "rec" into tmpfs.

Dependencies: should be installing ffmpeg with support mp4(h264)


Use:
```
$ git clone https://gitlab.com/screamager/rec_rtsp_ipcam.git
$ vi rec_rtsp_ipcam/config.yml # set your path to storage, add ip, url
$ rec_rtsp_ipcam/ipcamrec.py rec_rtsp_ipcam/config.yml
```

At running wil be create dirs and begin record streams.
