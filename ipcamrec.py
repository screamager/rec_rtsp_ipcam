#!/usr/bin/env python3

import os, sys
import threading
import time
import yaml
import errno
import subprocess
import signal
import shlex

CMD_FFMPEG  = 'ffmpeg -rtsp_transport tcp -fflags +genpts -i rtsp://'                     # Infile options
OPTS_FFMPEG = ' -c:v copy -an -hls_flags delete_segments -hls_list_size 200 -hls_time 2 ' # Outfile options

TIME         = 300 # Duration of video file
REMOTE_FILES = 10  # Count file to delete if size disk enfding
PERCENT_SIZE = 90  # Count size disk may be use

def currentTime ():
    return time.strftime("%Y-%m-%d_%H.%M.%S", time.localtime())

# Return numbers seconds from 1970 years
def curTimeStamp ():
    return int(time.time())

# Return True or False if host is available or not
def ping (host):
    return os.system('ping -c1 ' + host +' >/dev/null 2>&1') == 0

# Return dictionary from YAML file
class ParserYAML ():
    '''
    This is parser YAML file and translete to dictionary.
    '''
    def __init__ (self, yaml_file):
        self.yaml_file = yaml_file
    def parsYAML (self):
        '''
        It's return dictionary from YAML.
        '''
        self.dict_from_yaml = {}
        with open(self.yaml_file, 'r') as f:
            try:
                self.dict_from_yaml = yaml.load(f)
            except yaml.YAMLError as err:
                print(err)
        return self.dict_from_yaml

# Looking for size on disk
class CheckSizeDisk (ParserYAML):
    '''
    This class check for free size on disk.
    '''
    def __init__ (self, yaml_file):
        '''
        Initial path to storage
        '''
        ParserYAML.__init__(self, yaml_file)
        self.dict_from_yaml = self.parsYAML()
        self.path = self.dict_from_yaml['storage'].rstrip('/')
        self.remote_files = 10

    def checkSize (self):
        '''
        It's again calculate percent for this object.
        '''
        disk = os.statvfs(self.path)
        disk_f_buse = (disk.f_blocks - disk.f_bfree)
        self.percent = disk_f_buse * 100 // (disk_f_buse + disk.f_bavail) + 1
        return self.percent

    def rmOldFiles (self):
        '''
        It's remove old files and directories
        '''
        list_files = sorted([i for i in glob.iglob(self.path + '/*/*.mp4')],
                            key=lambda x: os.path.getmtime(x))
        os.remove(list_files[:self.remote_files])
        # Remove subdir is empty
        list(map(lambda x: os.rmdir(x),
                 (x for x in glob.glob('./*/*')
                      if os.path.isdir(x) and not os.listdir(x))))
        # Remove dir is empty exclude "rec" and "live"
        list(map(lambda x: os.rmdir(x),
                 (x for x in os.listdir(self.path + '/*')
                      if os.path.isdir(x) and not os.listdir(x) and x not in ['rec','live'])))

# Concantenation "m3u8" files to "mp4"
class ConcatVideoFiles ():
    '''
    This class concatenation files from "rec" and move given file to "date" dirs
    '''
    def __init__ (self, path, date):
        '''
        Extract day and hour from date string
        '''
        self.path = path
        self.date = date
        self.day = self.date.split('_')[0]
        self.hour = self.date.split('_')[1].split(':')[0]

    def tmpM3U8 (self):
        '''
        Create copy file rec.m3u8 to rec_tmp.m3u8
        '''
        self.f_m3u8 = self.path.rstrip('/') + '/rec/rec.m3u8'
        self.f_tmp_m3u8 = self.path.rstrip('/') + '/rec/rec_tmp.m3u8'
        if os.path.exists(self.f_m3u8):
            os.rename(self.f_m3u8, self.f_tmp_m3u8)
            with open(self.f_tmp_m3u8, 'a') as f:
                f.write('#EXT-X-ENDLIST')

    def mkDir (self):
        '''
        Create dirs for mp4 files
        '''
        self.dst_path = self.path.rstrip('/') + '/' + self.day + '/' + self.hour
        try:
            os.makedirs(self.dst_path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(self.dst_path):
                pass
            else:
                raise

    def concat (self):
        '''
        Run cmd ffmpeg for concatenation files to mp4
        '''
        obj_proc = subprocess.Popen(shlex.split('ffmpeg -y -i ' + self.f_tmp_m3u8
                  + ' -vcodec copy ' + self.dst_path + '/' + self.date + '.mp4'),
                  stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=False)
        obj_proc.wait()

    def rmOldTrFiles (self):
        '''
        Delete not needed ".rs" files
        '''
        if os.path.exists(self.f_tmp_m3u8):
            with open(self.f_tmp_m3u8) as f:
                for i in f.readlines():
                    if i[0] != '#':
                        if os.path.exists(self.path.rstrip('/') + '/rec/' + i.rstrip()):
                            os.remove(self.path.rstrip('/') + '/rec/' + i.rstrip())


class ConcatLoop (ParserYAML):
    '''
    Run cycle concatenations
    '''
    def __init__ (self, yaml_file):
        '''
        Initial path to storage
        '''
        ParserYAML.__init__(self, yaml_file)
        self.dict_from_yaml = self.parsYAML()
        self.path = self.dict_from_yaml['storage'].rstrip('/')
        self.exit = False #It is flag for  cycles break
        self.time = 300 # Duration of video file

    def _stampDict (self):
        '''
        Create hash table when key=<ip> and value=<next time for concatenation>
        '''
        self.interval = TIME - 5
        self.nearest_action = curTimeStamp() + self.interval
        return {i: self.nearest_action for i in list(self.dict_from_yaml['ip'].keys())}

    def concatLoop (self):
        '''
        Run cycle runing contatenations video files
        '''
        stamp_dict = self._stampDict()
        while not self.exit:
            nearest_action = curTimeStamp() + self.interval
            for i in stamp_dict:
                if stamp_dict[i] <= curTimeStamp():
                    next_action = curTimeStamp() + self.interval
                    time.sleep(8)
                    concat = ConcatVideoFiles(self.path + '/' + i,
                                              time.strftime('%Y.%m.%d_%H:%M:%S',
                                              time.localtime(next_action)))
                    concat.tmpM3U8()
                    concat.mkDir()
                    concat.concat()
                    concat.rmOldTrFiles()

                    stamp_dict[i] =  next_action
                else:
                    if stamp_dict[i] < nearest_action:
                        nearest_action = stamp_dict[i]
            delta_time = nearest_action - curTimeStamp()
            if delta_time >= 3 and not self.exit:
                # WAIT with check exit
                for i in range(60):
                    if not self.exit:
                        time.sleep((delta_time + 5)/60)

class Rec (ParserYAML):
    '''
    Run record rtsp stream
    '''
    def __init__ (self, yaml_file):
        '''
        Initial path to storage
        '''
        ParserYAML.__init__(self, yaml_file)
        self.dict_from_yaml = self.parsYAML()
        self.path = self.dict_from_yaml['storage'].rstrip('/')
        self.exit = False #It is flag for  cycles break
        self.list_thread = []
        self.list_procs = []
        self.cmd_ffmpeg = 'ffmpeg -rtsp_transport tcp -fflags +genpts -i rtsp://'
        self.opts_ffmpeg = ' -c:v copy -an -hls_flags delete_segments -hls_list_size 100 -hls_time 5 '

    def _mkDirs (self):
        '''
        Create dirs for record stream
        '''
        for i in list(self.dict_from_yaml['ip'].keys()):
            path_rec = self.path + '/' + i + '/rec'
            try:
                os.makedirs(path_rec)
            except OSError as exc:
                if exc.errno == errno.EEXIST and os.path.isdir(path_rec):
                    pass
                else:
                    raise

    def _videoStream (self, ip):
        '''
        Run record rtsp stream
        '''
        while not self.exit:
            if ping(ip):
                url = self.dict_from_yaml['url'][self.dict_from_yaml['ip'][ip]]['rtsp']
                self.list_procs.append(None)
                self.list_procs[-1] = subprocess.Popen(shlex.split(self.cmd_ffmpeg + ip + url +
                    self.opts_ffmpeg + self.path + '/' + ip + '/rec/rec.m3u8'),
                    stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=False)
                self.list_procs[-1].wait()

                # WAIT with check exit
                for i in range(6):
                    if not self.exit:
                        time.sleep(5)
            else:
                time.sleep(60)

    def rec (self):
        '''
        Run record all cameras into threads
        '''
        self._mkDirs()
        for i in list(self.dict_from_yaml['ip'].keys()):
                self.list_thread.append(None)
                self.list_thread[-1] = threading.Thread(target=self._videoStream, args=(i,))
                self.list_thread[-1].start()


if __name__ == '__main__':

    # Configuration YAML file. TODO add check is available argument
    try:
        sys.argv[1]
    except IndexError:
        print('Should be geted YAML configuration file as argument')
        sys.exit()
    else:
        conf_file = sys.argv[1]

    try:
        # Run record rtsp stream
        rec_video = Rec (conf_file)
        rec_video.cmd_ffmpeg = CMD_FFMPEG
        rec_video.opts_ffmpeg = OPTS_FFMPEG
        rec_video.rec()

        # Run cycle concatenation to mp4 files
        loop = ConcatLoop (conf_file)
        loop.time = TIME
        loop_thread = threading.Thread(target=loop.concatLoop)
        loop_thread.start()

        # Run cycle looking for use disk size
        check_size = CheckSizeDisk (conf_file)
        check_size.remote_files = REMOTE_FILES
        EXIT = False #It is flag for  cycles break
        while not EXIT:
            if check_size.checkSize() >= PERCENT_SIZE:
                check_size.rmOldFiles()
            else:
                # WAIT with check EXIT
                for i in range(6):
                    if not EXIT:
                        time.sleep(5)
    except (KeyboardInterrupt, SystemExit):
        EXIT = True
        rec_video.exit = True
        loop.exit = True
        (i.kill() for i in rec_video.list_procs  if i.pull())
        (i.join() for i in rec_video.list_thread  if i.isAlive())
        loop_thread.join()
        print('\n! Received keyboard interrupt, quitting threads.\n')
        sys.exit()
